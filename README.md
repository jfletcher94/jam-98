My submission for 98-Jam: https://itch.io/jam/98-jam

Theme: "Windows 98"

Challenge: "Rotation is the primary game mechanic."

My take on the Jam: Take control of a submarine whose only interface is a slow, dated Windows 98-like desktop. In order to properly control the submarine, you'll need to rotate between different windows: sonar, engine, steering, and diagnostics. Still a little rough around the edges!

There are no explicit instructions, but the information is all there if look for it. The interface might take some getting used to. It's an old system, so be patient!

Actually, there is one explicit instruction.

Locate the artefact.

Play here: https://jfletcher94.itch.io/submerged-98