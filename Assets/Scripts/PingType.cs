﻿public enum PingType {

    Friendly,
    Neutral,
    Hostile

}