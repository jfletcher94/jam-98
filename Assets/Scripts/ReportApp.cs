﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReportApp : App {
    
    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField] private Scrollbar scrollbar;

    public override void Activate() {
        gameObject.SetActive(true);
        scrollbar.value = 1f;
    }

    public override void Deactivate() {
        gameObject.SetActive(false);
    }

    public override string GetText() {
        return "diag_report_0.txt";
    }

    public override ApplicationWindow GetApplicationWindow() {
        return applicationWindow;
    }

}
