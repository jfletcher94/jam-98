﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Anomaly : MonoBehaviour {

    [SerializeField] private PingType pingType;
    [SerializeField] private SpriteRenderer ping;
    [SerializeField] private Transform center;

    private BoxCollider2D _collider;
    private float _coolDown = 0f;

    private void Awake() {
        _collider = GetComponent<BoxCollider2D>();
        var x = Random.Range(0, 2) == 0
            ? Random.Range(1f, SubmarineMovement.XBoundary)
            : Random.Range(-SubmarineMovement.XBoundary, -1f);
        var y = Random.Range(0, 2) == 0
            ? Random.Range(1f, SubmarineMovement.YBoundary)
            : Random.Range(-SubmarineMovement.YBoundary, -1f);
        transform.position = new Vector3(x, y, 0f);
    }

    public PingType GetPingType() {
        return pingType;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.gameObject.CompareTag("SonarArm")) {
            return;
        }

        _collider.enabled = false;
        _coolDown = 3f;
        Ping(other.gameObject.GetComponent<SonarArm>().GetSonarApp());
    }

    private void Ping(SonarApp sonarApp) {
        var position = transform.position;
        if (!sonarApp.ShortRangeOk || Vector2.Distance(position, center.position) > 1.2f) {
            ping.color = Color.white;
        } else {
            ping.color = GetPingColor(pingType);
        }
        if (!sonarApp.LongRangeOk) {
            var color = ping.color;
            ping.color = new Color(color.r, color.g, color.b, 0.5f);
        }
        if (sonarApp.CalibrationError != 0f) {
            position += Random.Range(0, 2) == 0
                ? new Vector3(sonarApp.CalibrationError, 0f, 0f)
                : new Vector3(0f, sonarApp.CalibrationError, 0f);
        }
        ping.transform.position = position;
    }

    private void Update() {
        if (_coolDown == 0f) {
            _collider.enabled = true;
            return;
        }

        _coolDown = Mathf.Max(0f, _coolDown - Time.deltaTime);
    }

    private static Color GetPingColor(PingType pingType) {
        switch (pingType) {
            case PingType.Friendly:
                return Color.cyan;
            case PingType.Hostile:
                return Color.red;
            default:
                return Color.white;
        }
    }

}
