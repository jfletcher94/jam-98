﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DesktopIcon : MonoBehaviour, IPointerClickHandler {

    [SerializeField] private WindowManager windowManager;
    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField, Tooltip("Seconds")] private float loadTime = 2f;

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.clickCount != 2) {
            return;
        }

        if (applicationWindow.toolbarButton.IsActive()) {
            windowManager.Open(applicationWindow);
            return;
        }
        
        windowManager.Load(applicationWindow, loadTime);
    }

}
