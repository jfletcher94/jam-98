﻿using TMPro;
using UnityEngine;

public class EngineApp : App {

    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField] private WindowManager windowManager;
    [SerializeField] private float loadTime = 1f;
    [SerializeField] private GameObject spinner;
    [SerializeField] private GameObject dial;
    [SerializeField] private SubmarineMovement submarineMovement;
    [SerializeField] private float accelerationScalar = 1f;
    [SerializeField] private float reverseScalar = 0.5f;

    [Header("Text Output")]
    [SerializeField]
    private TMP_Text direction;

    [SerializeField] private TMP_Text powerLevel;
    [SerializeField] private TMP_Text powerLevelChange;
    [SerializeField] private TMP_Text speed;
    [SerializeField] private TMP_Text speedChange;
    [SerializeField] private TMP_Text temperature;
    [SerializeField] private TMP_Text backup;
    [SerializeField] private TMP_Text propeller1;
    [SerializeField] private TMP_Text propeller2;

    private bool _loading;
    private float _seconds;
    private float _prevAcceleration;
    private float _prevSpeed;
    private SpeedSetting _speedSetting = SpeedSetting.Stop;
    private bool _backupEngaged = false;
    public bool TemperatureOk { get; set; } = true;
    public bool Propeller1Ok { get; set; } = true;
    public bool Propeller2Ok { get; set; } = true;
    public bool TemperatureFixable { get; set; } = true;
    public bool Propeller1Fixable { get; set; } = true;
    public bool Propeller2Fixable { get; set; } = true;

    public override void Activate() {
        gameObject.SetActive(true);
    }

    public override void Deactivate() {
        gameObject.SetActive(false);
    }

    public override string GetText() {
        return "Engine.exe";
    }

    public override ApplicationWindow GetApplicationWindow() {
        return applicationWindow;
    }

    public void Damage() {
        if (Random.Range(0, 4) == 0) {
            temperature.text = "#ERR";
            TemperatureOk = false;
        } else {
            propeller1.text = "#ERR";
            propeller2.text = "#ERR";
            if (Random.Range(0, 2) == 0) {
                Propeller1Ok = false;
            } else {
                Propeller2Ok = false;
            }
        }
    }

    public void RunDiagnostics(bool coolant, bool prop1, bool prop2) {
        if (coolant) {
            temperature.text = TemperatureOk ? "OK" : "FAIL";
        }
        if (prop1) {
            propeller1.text = Propeller1Ok ? "OK" : "FAIL";
        }
        if (prop2) {
            propeller2.text = Propeller2Ok ? "OK" : "FAIL";
        }
    }

    public bool EngageBackup() {
        if (_backupEngaged) {
            return false;
        }

        _backupEngaged = true;
        TemperatureOk = true;
        Propeller1Ok = true;
        Propeller2Ok = true;
        TemperatureFixable = true;
        Propeller1Fixable = true;
        Propeller2Fixable = true;
        backup.text = "Engaged";
        RunDiagnostics(true, true, true);
        return true;
    }

    public void UpdateValue(int value) {
        _speedSetting = (SpeedSetting) value;
        _loading = true;
        _seconds = loadTime * windowManager.GetDelayScalar();
        float power = GetPower(_speedSetting);
        if (!TemperatureOk || (!Propeller1Ok && !Propeller2Ok)) {
            power *= 0.2f;
        } else if (!Propeller1Ok || !Propeller2Ok) {
            power *= 0.5f;
        }
        submarineMovement.SetPower(power);
        spinner.SetActive(true);
        spinner.transform.rotation = Quaternion.identity;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        if (_loading) {
            if ((_seconds = Mathf.Max(0f, _seconds - Time.deltaTime)) == 0f) {
                _loading = false;
                spinner.SetActive(false);
                dial.transform.rotation = GetRotation(_speedSetting);
                Cursor.lockState = CursorLockMode.None;
            } else {
                return;
            }
        }

        var newAcceleration = submarineMovement.GetAcceleration();
        var newSpeed = submarineMovement.GetSpeed();
        if (newAcceleration > 0f) {
            direction.text = "FORE";
        } else if (newAcceleration < 0f) {
            direction.text = "BACK";
        } else {
            direction.text = "OFF";
        }
        powerLevel.text = $"{12f * newAcceleration:0.##}";
        powerLevelChange.text = $"{12f * (newAcceleration - _prevAcceleration) / Time.deltaTime:+0.##;-0.##}";
        speed.text = $"{12f * newSpeed:0.##}";
        speedChange.text = $"{12f * (newSpeed - _prevSpeed) / Time.deltaTime:+0.##;-0.##}";

        _prevAcceleration = newAcceleration;
        _prevSpeed = newSpeed;
    }

    private float GetPower(SpeedSetting speedSetting) {
        switch (speedSetting) {
            case SpeedSetting.Stop:
                return 0f;
            case SpeedSetting.AheadSlow:
                return 1f * accelerationScalar;
            case SpeedSetting.AheadHalf:
                return 2f * accelerationScalar;
            case SpeedSetting.AheadFull:
                return 4f * accelerationScalar;
            case SpeedSetting.AsternSlow:
                return -1f * accelerationScalar * reverseScalar;
            case SpeedSetting.AsternHalf:
                return -2f * accelerationScalar * reverseScalar;
            case SpeedSetting.AsternFull:
                return -4f * accelerationScalar * reverseScalar;
            default:
                return 0f;
        }
    }

    private static Quaternion GetRotation(SpeedSetting speedSetting) {
        switch (speedSetting) {
            case SpeedSetting.Stop:
                return Quaternion.identity;
            case SpeedSetting.AheadSlow:
                return Quaternion.Euler(0f, 0f, -30f);
            case SpeedSetting.AheadHalf:
                return Quaternion.Euler(0f, 0f, -60f);
            case SpeedSetting.AheadFull:
                return Quaternion.Euler(0f, 0f, -90f);
            case SpeedSetting.AsternSlow:
                return Quaternion.Euler(0f, 0f, 30f);
            case SpeedSetting.AsternHalf:
                return Quaternion.Euler(0f, 0f, 60f);
            case SpeedSetting.AsternFull:
                return Quaternion.Euler(0f, 0f, 90f);
            default:
                return Quaternion.identity;
        }
    }

    private enum SpeedSetting {

        Stop,
        AheadSlow,
        AheadHalf,
        AheadFull,
        AsternSlow,
        AsternHalf,
        AsternFull,

    }

}
