﻿using System.Text;
using TMPro;
using UnityEngine;

public class DiagnosticsApp : App {

    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField] private WindowManager windowManager;
    [SerializeField] private float loadTime = 0.5f;
    [SerializeField] private ApplicationWindow reportApplicationWindow;
    [SerializeField] private CompleteIcon completeIcon;
    [SerializeField] private SonarApp sonarApp;
    [SerializeField] private EngineApp engineApp;
    [SerializeField] private SteeringApp steeringApp;
    [SerializeField] private TMP_Text text;

    // Sonar
    private bool _longRange;
    private bool _shortRange;
    private bool _calibration;

    // Engine
    private bool _prop1;
    private bool _prop2;
    private bool _coolant;

    // Steering
    private bool _hydraulics;
    private bool _stabilizers;
    private bool _compass;

    public void LongRange(bool selected) {
        _longRange = selected;
    }

    public void ShortRange(bool selected) {
        _shortRange = selected;
    }

    public void Calibration(bool selected) {
        _calibration = selected;
    }

    public void Prop1(bool selected) {
        _prop1 = selected;
    }

    public void Prop2(bool selected) {
        _prop2 = selected;
    }

    public void Coolant(bool selected) {
        _coolant = selected;
    }

    public void Hydraulics(bool selected) {
        _hydraulics = selected;
    }

    public void Stabilizers(bool selected) {
        _stabilizers = selected;
    }

    public void Compass(bool selected) {
        _compass = selected;
    }

    public void RunDiagnostics() {
        int systemCount = 0;
        StringBuilder stringBuilder =
            new StringBuilder("-~*Submarine Doctor (C) Diagnostic Report*~-\nversion 0.21.6\n");
        stringBuilder.Append("\n\n====Computer System====\n");
        stringBuilder.Append("Status: ONLINE\n");
        stringBuilder.Append("OS integrity: OK\n");
        stringBuilder.Append($"% CPU: {10 + windowManager.GetOpenWindows() * 15 + Random.Range(0, 14)}\n");
        stringBuilder.Append($"% Memory: {20 + windowManager.GetOpenWindows() * 25 + Random.Range(0, 25)}\n\n");
        stringBuilder.Append($"Recent crashes: {windowManager.NumCrashes}\n");
        if (windowManager.NumCrashes > 0) {
            stringBuilder.Append("Cause of crash: OOM\n");
        }

        stringBuilder.Append("\n\n====Anomalies====\n");
        Anomaly anomaly = sonarApp.GetNearestAnomaly();
        if (anomaly == null) {
            stringBuilder.Append("No anomalies detected\n");
        } else {
            systemCount += 2;
            if (anomaly.GetPingType() == PingType.Friendly) {
                completeIcon.DiagnosticsRan = true;
            }
            stringBuilder.Append("WARNING: anomaly detected!\nScanning anomaly...\n~!~ ATTENTION:\n");
            stringBuilder.Append(
                anomaly.GetPingType() == PingType.Friendly
                    ? "~!~ ██████████ artefact located\n"
                    : "~!~ Hostile entity detected\n~!~ DO NOT APPROACH\n");
        }

        stringBuilder.Append("\n\n====Engine System====\n");
        stringBuilder.Append(
            engineApp.TemperatureOk && engineApp.Propeller1Ok && engineApp.Propeller2Ok
                ? "Status: ONLINE\n"
                : "Status: ERR\n");
        stringBuilder.Append(
            engineApp.GetApplicationWindow().gameObject.activeSelf
                ? "Engine-o-Matic (C) digital interface: ONLINE\n"
                : "Engine-o-Matic (C) digital interface: READY\n");
        if (_coolant) {
            systemCount++;
            if (engineApp.TemperatureOk) {
                stringBuilder.Append("Coolant: OK\n");
            } else {
                if (engineApp.TemperatureFixable && Random.Range(0, 2) == 0) {
                    engineApp.TemperatureOk = true;
                    VerboseDiagnostics(stringBuilder, "Coolant", true, "OK");
                } else {
                    engineApp.TemperatureFixable = false;
                    VerboseDiagnostics(stringBuilder, "Coolant", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Coolant: SKIP\n");
        }
        if (_prop1) {
            systemCount++;
            if (engineApp.Propeller1Ok) {
                stringBuilder.Append("Propeller 1: OK\n");
            } else {
                if (engineApp.Propeller1Fixable && Random.Range(0, 2) == 0) {
                    engineApp.Propeller1Ok = true;
                    VerboseDiagnostics(stringBuilder, "Propeller 1", true, "OK");
                } else {
                    engineApp.Propeller1Fixable = false;
                    VerboseDiagnostics(stringBuilder, "Propeller 1", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Propeller 1: SKIP\n");
        }
        if (_prop2) {
            systemCount++;
            if (engineApp.Propeller2Ok) {
                stringBuilder.Append("Propeller 2: OK\n");
            } else {
                if (engineApp.Propeller2Fixable && Random.Range(0, 2) == 0) {
                    engineApp.Propeller2Ok = true;
                    VerboseDiagnostics(stringBuilder, "Propeller 2", true, "OK");
                } else {
                    engineApp.Propeller2Fixable = false;
                    VerboseDiagnostics(stringBuilder, "Propeller 2", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Propeller 2: SKIP\n");
        }
        if ((_coolant && !engineApp.TemperatureOk) ||
            (_prop1 && _prop2 && !engineApp.Propeller1Ok && !engineApp.Propeller2Ok)) {
            systemCount += 2;
            stringBuilder.Append("WARNING: Engine critical. Engaging backup.\n");
            stringBuilder.Append("Starting backup system...\n");
            if (engineApp.EngageBackup()) {
                stringBuilder.Append("Success!\n");
                stringBuilder.Append("Coolant: OK\n");
                stringBuilder.Append("Propeller 1: OK\n");
                stringBuilder.Append("Propeller 2: OK\n");
                stringBuilder.Append("Backup: Engaged\n");
            } else {
                stringBuilder.Append("Failure! Backup already engaged!\n");
                stringBuilder.Append("WARNING: Engine critical. Backup unavailable.\n");
            }
        }

        stringBuilder.Append("\n\n====Steering System====\n");
        stringBuilder.Append(
            steeringApp.HydraulicsOk && steeringApp.StabilizerError == 0f && steeringApp.CompassError == 0f
                ? "Status: ONLINE\n"
                : "Status: ERR\n");
        stringBuilder.Append(
            steeringApp.GetApplicationWindow().gameObject.activeSelf
                ? "Steer Pro 98 (C) digital interface: ONLINE\n"
                : "Steer Pro 98 (C) digital interface: READY\n");
        if (_hydraulics) {
            systemCount++;
            if (steeringApp.HydraulicsOk) {
                stringBuilder.Append("Hydraulics: OK\n");
            } else {
                if (steeringApp.HydraulicsFixable && Random.Range(0, 3) != 0) {
                    steeringApp.HydraulicsOk = true;
                    VerboseDiagnostics(stringBuilder, "Hydraulics", true, "OK");
                } else {
                    steeringApp.HydraulicsFixable = false;
                    VerboseDiagnostics(stringBuilder, "Hydraulics", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Hydraulics: SKIP\n");
        }
        if (_stabilizers) {
            systemCount++;
            if (steeringApp.StabilizerError == 0f) {
                stringBuilder.Append("Stabilizers: OK\n");
            } else {
                if (Random.Range(0, 2) == 0) {
                    steeringApp.StabilizerError /= 2f;
                    VerboseDiagnostics(stringBuilder, "Stabilizers", true, "PARTIAL");
                } else {
                    VerboseDiagnostics(stringBuilder, "Stabilizers", false, "PARTIAL");
                }
            }
        } else {
            stringBuilder.Append("Stabilizers: SKIP\n");
        }
        if (_compass) {
            systemCount++;
            if (steeringApp.CompassError == 0f) {
                stringBuilder.Append("Compass: OK\n");
            } else {
                if (Random.Range(0, 2) == 0) {
                    steeringApp.CompassError /= 2f;
                    VerboseDiagnostics(stringBuilder, "Compass", true, "PARTIAL");
                } else {
                    VerboseDiagnostics(stringBuilder, "Compass", false, "PARTIAL");
                }
            }
        } else {
            stringBuilder.Append("Compass: SKIP\n");
        }

        stringBuilder.Append("\n\n====Sonar System====\n");
        stringBuilder.Append(
            sonarApp.LongRangeOk && sonarApp.ShortRangeOk && sonarApp.CalibrationError == 0f
                ? "Status: ONLINE\n"
                : "Status: ERR\n");
        stringBuilder.Append(
            sonarApp.GetApplicationWindow().gameObject.activeSelf
                ? "Sonar Master 98 (C) digital interface: ONLINE\n"
                : "Sonar Master 98 (C) digital interface: READY\n");
        if (_longRange) {
            systemCount++;
            if (sonarApp.LongRangeOk) {
                stringBuilder.Append("Long Range: OK\n");
            } else {
                if (sonarApp.LongRangeFixable && Random.Range(0, 3) != 0) {
                    sonarApp.LongRangeOk = true;
                    VerboseDiagnostics(stringBuilder, "Long Range", true, "OK");
                } else {
                    sonarApp.LongRangeFixable = false;
                    VerboseDiagnostics(stringBuilder, "Long Range", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Long Range: SKIP\n");
        }
        if (_shortRange) {
            systemCount++;
            if (sonarApp.ShortRangeOk) {
                stringBuilder.Append("Short Range: OK\n");
            } else {
                if (sonarApp.ShortRangeFixable && Random.Range(0, 3) != 0) {
                    sonarApp.ShortRangeOk = true;
                    VerboseDiagnostics(stringBuilder, "Short Range", true, "OK");
                } else {
                    sonarApp.ShortRangeFixable = false;
                    VerboseDiagnostics(stringBuilder, "Short Range", false, "FAIL");
                }
            }
        } else {
            stringBuilder.Append("Short Range: SKIP\n");
        }
        if (_calibration) {
            systemCount++;
            if (sonarApp.CalibrationError == 0f) {
                stringBuilder.Append("Calibration: OK\n");
            } else {
                if (Random.Range(0, 2) == 0) {
                    sonarApp.CalibrationError /= 2f;
                    VerboseDiagnostics(stringBuilder, "Calibration", true, "PARTIAL");
                } else {
                    VerboseDiagnostics(stringBuilder, "Calibration", false, "PARTIAL");
                }
            }
        } else {
            stringBuilder.Append("Calibration: SKIP\n");
        }

        engineApp.RunDiagnostics(_coolant, _prop1, _prop2);
        steeringApp.RunDiagnostics(_hydraulics, _stabilizers, _compass);
        text.text = stringBuilder.ToString();
        windowManager.Close(reportApplicationWindow);
        windowManager.Load(reportApplicationWindow, systemCount * loadTime);
    }

    public override void Activate() {
        gameObject.SetActive(true);
    }

    public override void Deactivate() {
        gameObject.SetActive(false);
    }

    public override string GetText() {
        return "Diagnostics.exe";
    }

    public override ApplicationWindow GetApplicationWindow() {
        return applicationWindow;
    }

    private static void VerboseDiagnostics(
        StringBuilder stringBuilder,
        string component,
        bool restartSuccess,
        string message) {
        stringBuilder.Append($"{component}: ERR\n");
        stringBuilder.Append("Running diagnostics...\n");
        stringBuilder.Append("Error detected...\n");
        stringBuilder.Append("Restarting system...\n");
        stringBuilder.Append("Restart complete...\n");
        stringBuilder.Append("Running diagnostics...\n");
        stringBuilder.Append("Diagnostics complete!\n");
        if (restartSuccess) {
            stringBuilder.Append("Source of error: Hardware malfunction\n");
            stringBuilder.Append($"{component}: {message}\n");
        } else {
            stringBuilder.Append("Source of error: Hardware damaged\n");
            stringBuilder.Append($"{component}: {message}\n");
        }
    }

}
