﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private SubmarineMovement submarineMovement;
    [SerializeField] private float chaseRange = 1.4f;
    [SerializeField] private float chaseSpeed = 0.04f;
    [SerializeField] private float attackRange = 0.25f;
    const float AttackCooldown = 10f;

    private float attack = 0f;

    private void Update() {
        if (attack > 0f) {
            attack = Mathf.Max(0f, attack - Time.deltaTime);
            return;
        }

        float distance = Vector2.Distance(transform.position, submarineMovement.transform.position);
        if (distance > chaseRange) {
            return;
        }

        if (distance <= attackRange) {
            submarineMovement.Damage();
            attack = AttackCooldown;
            return;
        }

        transform.position = Vector2.MoveTowards(
            transform.position,
            submarineMovement.transform.position,
            chaseSpeed * Time.deltaTime);
    }

}
