﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class CompleteIcon : MonoBehaviour, IPointerClickHandler {

    [SerializeField] private WindowManager windowManager;
    [SerializeField] private Transform center;
    [SerializeField] private Transform artefact;
    [SerializeField] private GameObject end;
    [SerializeField] private TMP_Text text;

    public bool DiagnosticsRan { get; set; }

    private bool _closeToArtefact = false;

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.clickCount != 2) {
            return;
        }

        windowManager.CloseAll();
        end.SetActive(true);
        if (_closeToArtefact && DiagnosticsRan) {
            text.text = "You win!";
        } else {
            text.text = "You Lose!";
        }
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        _closeToArtefact = Vector2.Distance(center.position, artefact.position) < 1.5f;
        if (!_closeToArtefact) {
            DiagnosticsRan = false;
        }
    }

}
