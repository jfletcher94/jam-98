﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WindowManager : MonoBehaviour {

    [SerializeField] private GameObject window;
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject spinner;
    [SerializeField] private TMP_Text titleText;
    [SerializeField] private Sprite buttonUp;
    [SerializeField] private Sprite buttonDown;
    [SerializeField] private ApplicationWindow[] allApplicationWindows;

    private ApplicationWindow _applicationWindow;
    private bool _loading = false;
    private float _seconds;
    private int _openWindows = 0;
    public int NumCrashes { get; set; } = 0;

    public int GetOpenWindows() {
        return _openWindows;
    }

    public float GetDelayScalar() {
        // TODO tune this to be harsher for more windows
        return 0.5f * 1f * _openWindows;
    }

    public void Close() {
        Close(_applicationWindow);
        _applicationWindow = null;
    }

    public void Close(ApplicationWindow applicationWindow) {
        if (!applicationWindow.gameObject.activeSelf) {
            return;
        }

        _openWindows--;
        applicationWindow.gameObject.SetActive(false);
        Minimize(applicationWindow);
    }

    public void CloseAll() {
        NumCrashes++;
        foreach (var applicationWindow in allApplicationWindows) {
            Close(applicationWindow);
        }
    }

    public void Minimize() {
        Minimize(_applicationWindow);
        _applicationWindow = null;
    }
    public void Minimize(ApplicationWindow applicationWindow) {
        applicationWindow.toolbarButton.image.sprite = buttonUp;
        applicationWindow.app.Deactivate();
        window.gameObject.SetActive(false);
    }

    public void Load(ApplicationWindow applicationWindow, float seconds) {
        _openWindows++;
        OpenHelper(applicationWindow);
        applicationWindow.transform.SetAsLastSibling();
        _loading = true;
        _seconds = seconds * GetDelayScalar();
        panel.SetActive(true);
        spinner.SetActive(true);
        spinner.transform.rotation = Quaternion.identity;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Open(ApplicationWindow applicationWindow) {
        if (OpenHelper(applicationWindow)) {
            _applicationWindow.app.Activate();
        }
    }

    private bool OpenHelper(ApplicationWindow applicationWindow) {
        if (applicationWindow == _applicationWindow) {
            Minimize();
            return false;
        }

        if (_applicationWindow != null) {
            Minimize();
        }

        window.gameObject.SetActive(true);
        _applicationWindow = applicationWindow;
        _applicationWindow.gameObject.SetActive(true);
        _applicationWindow.toolbarButton.image.sprite = buttonDown;
        titleText.text = _applicationWindow.app.GetText();
        return true;
    }

    private void Update() {
        if (!_loading || (_seconds -= Time.deltaTime) > 0f) {
            return;
        }

        _loading = false;
        spinner.SetActive(false);
        _applicationWindow.app.Activate();
        Cursor.lockState = CursorLockMode.None;
        switch (_openWindows) {
            case 4: {
                if (Random.Range(0, 5) == 0) {
                    CloseAll();
                }
                break;
            }
            case 5: {
                if (Random.Range(0, 2) == 0) {
                    CloseAll();
                }
                break;
            }
        }
    }

}
