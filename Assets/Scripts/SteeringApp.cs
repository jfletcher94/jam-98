﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SteeringApp : App {

    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField] private WindowManager windowManager;
    [SerializeField] private float loadTime = 1f;
    [SerializeField] private GameObject spinner;
    [SerializeField] private TMP_Dropdown dropdown;
    [SerializeField] private SubmarineMovement submarineMovement;

    [Header("Text Output")]
    [SerializeField]
    private TMP_Text rudderPosition;

    [SerializeField] private TMP_Text rudderChange;
    [SerializeField] private TMP_Text bearingDegrees;
    [SerializeField] private TMP_Text bearingDegreesChange;
    [SerializeField] private TMP_Text bearingCardinalDirection;
    [SerializeField] private TMP_Text hydraulicsText;
    [SerializeField] private TMP_Text stabilizersText;
    [SerializeField] private TMP_Text compassText;

    private bool _loading;
    private float _seconds;
    private float _prevRudderPosition;
    private float _prevBearingDegrees;
    public bool HydraulicsOk { get; set; } = true;
    public bool HydraulicsFixable { get; set; } = true;
    public float StabilizerError { get; set; } = 0f;
    public float CompassError { get; set; } = 0f;

    public override void Activate() {
        gameObject.SetActive(true);
    }

    public override void Deactivate() {
        gameObject.SetActive(false);
    }

    public override string GetText() {
        return "Steering.exe";
    }

    public override ApplicationWindow GetApplicationWindow() {
        return applicationWindow;
    }

    public void Damage() {
        if (Random.Range(0, 5) == 0) {
            hydraulicsText.text = "#ERR";
            stabilizersText.text = "#ERR";
            HydraulicsOk = false;
        } else {
            stabilizersText.text = "#ERR";
            compassText.text = "#ERR";
            if (Random.Range(0, 2) == 0) {
                if (StabilizerError == 0f) {
                    StabilizerError = Random.Range(0, 2) == 0 ? 0.5f : -0.5f;
                } else {
                    StabilizerError *= 1.5f;
                }
            } else {
                if (CompassError == 0f) {
                    CompassError = Random.Range(0, 2) == 0 ? 10f : -10f;
                } else {
                    CompassError *= 1.5f;
                }
            }
        }
    }

    public void RunDiagnostics(bool hydraulics, bool stabilizers, bool compass) {
        if (hydraulics) {
            hydraulicsText.text = HydraulicsOk ? "OK" : "FAIL";
        }
        if (stabilizers) {
            stabilizersText.text = StabilizerError == 0f ? "OK" : "PARTIAL";
        }
        if (compass) {
            compassText.text = CompassError == 0f ? "OK" : "PARTIAL";
        }
    }

    public void UpdateValue(int value) {
        float bearing = int.Parse(dropdown.options[value].text);
        if (!HydraulicsOk) {
            bearing *= 0.33f;
        }
        bearing += StabilizerError;
        submarineMovement.SetBearing(bearing);
        _loading = true;
        _seconds = loadTime * windowManager.GetDelayScalar();
        spinner.SetActive(true);
        spinner.transform.rotation = Quaternion.identity;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        if (_loading) {
            if ((_seconds = Mathf.Max(0f, _seconds - Time.deltaTime)) == 0f) {
                _loading = false;
                spinner.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
            } else {
                return;
            }
        }

        var newRudderPosition = submarineMovement.GetTurn();
        var newBearingDegrees = submarineMovement.GetRotation().eulerAngles.z + CompassError;

        rudderPosition.text = $"{newRudderPosition:0.##}";
        rudderChange.text = $"{(newRudderPosition - _prevRudderPosition) / Time.deltaTime:+0.##;-0.##}";
        bearingDegrees.text = $"{newBearingDegrees:0.##}";
        bearingDegreesChange.text = $"{(newBearingDegrees - _prevBearingDegrees) / Time.deltaTime:+0.##;-0.##}";
        if (newBearingDegrees < 11.25) {
            bearingCardinalDirection.text = "N";
        } else if (newBearingDegrees < 33.75) {
            bearingCardinalDirection.text = "NNE";
        } else if (newBearingDegrees < 56.25) {
            bearingCardinalDirection.text = "NE";
        } else if (newBearingDegrees < 78.75) {
            bearingCardinalDirection.text = "ENE";
        } else if (newBearingDegrees < 101.25) {
            bearingCardinalDirection.text = "E";
        } else if (newBearingDegrees < 123.75) {
            bearingCardinalDirection.text = "ESE";
        } else if (newBearingDegrees < 146.25) {
            bearingCardinalDirection.text = "SE";
        } else if (newBearingDegrees < 168.75) {
            bearingCardinalDirection.text = "SSE";
        } else if (newBearingDegrees < 191.25) {
            bearingCardinalDirection.text = "S";
        } else if (newBearingDegrees < 213.75) {
            bearingCardinalDirection.text = "SSW";
        } else if (newBearingDegrees < 236.25) {
            bearingCardinalDirection.text = "SW";
        } else if (newBearingDegrees < 258.75) {
            bearingCardinalDirection.text = "WSW";
        } else if (newBearingDegrees < 281.25) {
            bearingCardinalDirection.text = "W";
        } else if (newBearingDegrees < 303.75) {
            bearingCardinalDirection.text = "WNW";
        } else if (newBearingDegrees < 326.25) {
            bearingCardinalDirection.text = "NW";
        } else if (newBearingDegrees < 348.75) {
            bearingCardinalDirection.text = "NNW";
        } else {
            bearingCardinalDirection.text = "N";
        }

        _prevRudderPosition = newRudderPosition;
        _prevBearingDegrees = newBearingDegrees;
    }

}
