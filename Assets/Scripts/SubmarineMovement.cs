﻿using UnityEngine;

public class SubmarineMovement : MonoBehaviour {

    public const float XBoundary = 12;
    public const float YBoundary = 9;

    [SerializeField] private Transform submarineTransform;
    [SerializeField] private SonarApp sonarApp;
    [SerializeField] private EngineApp engineApp;
    [SerializeField] private SteeringApp steeringApp;
    [SerializeField] private AudioSource engineSound;
    [SerializeField] private AudioSource explosionSound;
    [SerializeField] private float turnAcceleration = 1f;
    [SerializeField] private float turnSpeed = 1f;
    [SerializeField] private float powerAcceleration = 1f;
    [SerializeField] private float speedScalar = 1f;
    [SerializeField] private float dragScalar = 1f;
    [SerializeField] private float minSpeed = 0.01f;
    private float _bearing;
    private float _turn;
    private float _power;
    private float _acceleration;
    private float _speed;

    public void SetBearing(float bearing) {
        _bearing = bearing;
    }

    public float GetTurn() {
        return _turn;
    }

    public void SetPower(float power) {
        _power = power;
    }

    public float GetSpeed() {
        return _speed;
    }

    public float GetAcceleration() {
        return _acceleration;
    }

    public Quaternion GetRotation() {
        return Quaternion.Inverse(submarineTransform.rotation);
    }

    public void Damage() {
        explosionSound.Play();
        switch (Random.Range(0, 3)) {
            case 0:
                sonarApp.Damage();
                break;
            case 1:
                engineApp.Damage();
                break;
            case 2:
                steeringApp.Damage();
                break;
        }
    }

    private void Update() {
        if (_bearing > _turn) {
            _turn = Mathf.Min(_bearing, _turn + turnAcceleration * Time.deltaTime);
        } else if (_bearing < _turn) {
            _turn = Mathf.Max(_bearing, _turn - turnAcceleration * Time.deltaTime);
        }

        if (_power > _acceleration) {
            _acceleration = Mathf.Min(_power, _acceleration + powerAcceleration * Time.deltaTime);
        } else if (_power < _acceleration) {
            _acceleration = Mathf.Max(_power, _acceleration - powerAcceleration * Time.deltaTime);
        }
        float drag = minSpeed + _speed * _speed * dragScalar;
        if (_speed < 0f) {
            drag = -drag;
        }
        _speed += (_acceleration - drag) * Time.deltaTime;
        if (_acceleration == 0f && Mathf.Abs(_speed) < minSpeed) {
            _speed = 0f;
        }

        if (_turn != 0f) {
            submarineTransform.Rotate(Vector3.back, _turn * turnSpeed * Time.deltaTime);
        }
        if (_speed != 0f) {
            submarineTransform.Translate(Vector3.up * (_speed * speedScalar * Time.deltaTime));
        }

        var position = submarineTransform.position;
        if (position.x > XBoundary) {
            submarineTransform.position = new Vector3(position.x - 2 * XBoundary, position.y, position.z);
        } else if (position.x < -XBoundary) {
            submarineTransform.position = new Vector3(position.x + 2 * XBoundary, position.y, position.z);
        } else if (position.y > YBoundary) {
            submarineTransform.position = new Vector3(position.x, position.y - 2 * YBoundary, position.z);
        } else if (position.y < -YBoundary) {
            submarineTransform.position = new Vector3(position.x, position.y + 2 * YBoundary, position.z);
        }

        float engineSoundScalar = (_acceleration >= 0f ? _acceleration : _acceleration * -2f) * 1.5f;
        engineSound.volume = 0.15f + engineSoundScalar;
        engineSound.pitch = 1f + engineSoundScalar;
    }

}
