﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOnCamera : MonoBehaviour {

    private Camera _camera;

    private void Awake() {
        _camera = Camera.main;
    }

    private void Update() {
        var myTransform = transform;
        var position = myTransform.position;
        myTransform.position = position;
        _camera.transform.position = new Vector3(position.x, position.y, -10f);
    }

}
