﻿using System;
using UnityEngine;

public class Ping : MonoBehaviour {

    [SerializeField] private PingType pingType = PingType.Neutral;
    [SerializeField] private float decayRate = 0.1f;

    private Camera _camera;
    private SpriteRenderer _spriteRenderer;

    private void Awake() {
        _camera = Camera.main;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        switch (pingType) {
            case PingType.Friendly:
                _spriteRenderer.color = Color.cyan;
                break;
            case PingType.Neutral:
                _spriteRenderer.color = Color.white;
                break;
            case PingType.Hostile:
                _spriteRenderer.color = Color.red;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        var color = _spriteRenderer.color;
        _spriteRenderer.color = new Color(color.r, color.g, color.b, 0f);
    }

    private void Update() {
        transform.rotation = _camera.transform.rotation;
        float alpha = _spriteRenderer.color.a;
        if (alpha == 0f) {
            return;
        }
        
        alpha = Mathf.Max(0f, alpha - decayRate * Time.deltaTime);
        var color = _spriteRenderer.color;
        _spriteRenderer.color = new Color(color.r, color.g, color.b, alpha);
    }

}
