﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarArm : MonoBehaviour {

    [SerializeField] private SonarApp sonarApp;
    [SerializeField] private float rotationSpeed = 0.1f;

    public SonarApp GetSonarApp() {
        return sonarApp;
    }

    private void Update() {
        Transform myTransform = transform;
        myTransform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
        myTransform.localPosition = Vector3.zero;
    }

}
