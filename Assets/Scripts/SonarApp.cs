﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class SonarApp : App {

    [SerializeField] private ApplicationWindow applicationWindow;
    [SerializeField] private AudioSource sonarBeep;
    [SerializeField] private GameObject background;
    [SerializeField] private GameObject panel;
    [SerializeField] private Transform center;
    [SerializeField] private SubmarineMovement submarineMovement;
    [SerializeField] private List<Anomaly> anomalies;
    [SerializeField] private float maxDetectableAnomalyDistance = 1.5f;
    [Header("Arrows")] [SerializeField] private GameObject forwardArrow1;
    [SerializeField] private GameObject forwardArrow2;
    [SerializeField] private GameObject forwardArrow3;
    [SerializeField] private GameObject backwardArrow1;
    [SerializeField] private GameObject backwardArrow2;
    [SerializeField] private GameObject leftArrow;
    [SerializeField] private GameObject rightArrow;

    public bool LongRangeOk { get; set; } = true;
    public bool ShortRangeOk { get; set; } = true;
    public float CalibrationError { get; set; } = 0f;
    public bool LongRangeFixable { get; set; } = true;
    public bool ShortRangeFixable { get; set; } = true;

    public override void Activate() {
        sonarBeep.volume = 1f;
        background.SetActive(false);
        panel.SetActive(false);
    }

    public override void Deactivate() {
        sonarBeep.volume = 0.25f;
        background.SetActive(true);
        panel.SetActive(true);
    }

    public override string GetText() {
        return "Sonar.exe";
    }

    public override ApplicationWindow GetApplicationWindow() {
        return applicationWindow;
    }

    public void Damage() {
        if (Random.Range(0, 4) == 0) {
            if (Random.Range(0, 2) == 0) {
                LongRangeOk = false;
            } else {
                ShortRangeOk = false;
            }
        } else {
            if (CalibrationError == 0f) {
                CalibrationError = Random.Range(0, 2) == 0 ? 0.05f : -0.05f;
            } else {
                CalibrationError *= 1.5f;
            }
        }
    }

    public Anomaly GetNearestAnomaly() {
        Anomaly closest = null;
        float minDistance = float.MaxValue;
        foreach (var anomaly in anomalies) {
            float distance = Vector2.Distance(center.position, anomaly.transform.position);
            if (distance < maxDetectableAnomalyDistance && distance < minDistance) {
                minDistance = distance;
                closest = anomaly;
            }
        }
        return closest;
    }

    private void Update() {
        if (submarineMovement.GetTurn() < 0f) {
            leftArrow.SetActive(true);
            rightArrow.SetActive(false);
        } else if (submarineMovement.GetTurn() > 0f) {
            leftArrow.SetActive(false);
            rightArrow.SetActive(true);
        } else {
            leftArrow.SetActive(false);
            rightArrow.SetActive(false);
        }
        if (submarineMovement.GetSpeed() < 0f) {
            forwardArrow1.SetActive(false);
            forwardArrow2.SetActive(false);
            forwardArrow3.SetActive(false);
            backwardArrow1.SetActive(true);
            backwardArrow2.SetActive(submarineMovement.GetSpeed() < -0.45f);
        } else if (submarineMovement.GetSpeed() > 0f) {
            forwardArrow1.SetActive(true);
            forwardArrow2.SetActive(submarineMovement.GetSpeed() > 0.45f);
            forwardArrow3.SetActive(submarineMovement.GetSpeed() > 0.65f);
            backwardArrow1.SetActive(false);
            backwardArrow2.SetActive(false);
        } else {
            forwardArrow1.SetActive(false);
            forwardArrow2.SetActive(false);
            forwardArrow3.SetActive(false);
            backwardArrow1.SetActive(false);
            backwardArrow2.SetActive(false);
        }
    }

}
