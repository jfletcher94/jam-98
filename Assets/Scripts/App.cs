﻿using UnityEngine;

public abstract class App : MonoBehaviour {

    public abstract void Activate();

    public abstract void Deactivate();

    public abstract string GetText();

    public abstract ApplicationWindow GetApplicationWindow();

}
